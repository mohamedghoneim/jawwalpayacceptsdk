# JawwalPayAcceptSDK

[![CI Status](https://img.shields.io/travis/mohamedghoneim/JawwalPayAcceptSDK.svg?style=flat)](https://travis-ci.org/mohamedghoneim/JawwalPayAcceptSDK)
[![Version](https://img.shields.io/cocoapods/v/JawwalPayAcceptSDK.svg?style=flat)](https://cocoapods.org/pods/JawwalPayAcceptSDK)
[![License](https://img.shields.io/cocoapods/l/JawwalPayAcceptSDK.svg?style=flat)](https://cocoapods.org/pods/JawwalPayAcceptSDK)
[![Platform](https://img.shields.io/cocoapods/p/JawwalPayAcceptSDK.svg?style=flat)](https://cocoapods.org/pods/JawwalPayAcceptSDK)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

JawwalPayAcceptSDK is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'JawwalPayAcceptSDK'
```

## Author

mohamedghoneim, mohamedghoneim@weaccept.co

## License

JawwalPayAcceptSDK is available under the MIT license. See the LICENSE file for more info.
